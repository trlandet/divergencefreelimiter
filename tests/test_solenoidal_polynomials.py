import dolfin
from solenoidal import SolenoidalPolynomials


def setup_solpol(u_cpp, v_cpp):
    mesh = dolfin.UnitSquareMesh(4, 4)
    ue = dolfin.Expression(u_cpp, degree=2)
    ve = dolfin.Expression(v_cpp, degree=2)

    V = dolfin.FunctionSpace(mesh, 'DG', 2)
    u = dolfin.interpolate(ue, V)
    v = dolfin.interpolate(ve, V)

    solpol = SolenoidalPolynomials(V)
    return solpol, u, v


def verify_solpol(solpol, u, v, eps=1e-14):
    V = u.function_space()
    u2 = dolfin.Function(V)
    v2 = dolfin.Function(V)

    solpol.set_from_lagrange(u, v)
    solpol.get_lagrange(u2, v2)

    eu = dolfin.errornorm(u, u2, degree_rise=0)
    ev = dolfin.errornorm(v, v2, degree_rise=0)
    print 'eu, ev:', eu, ev

    assert eu < eps
    assert ev < eps


def test_const_vel():
    solpol, u, v = setup_solpol('2', '5')
    verify_solpol(solpol, u, v)

def test_zero_vel():
    solpol, u, v = setup_solpol('0', '0')
    verify_solpol(solpol, u, v)

def test_quadratic():
    solpol, u, v = setup_solpol('5*x[0]*x[0] + 4*x[1]*x[1] + 3*x[0]*x[1] + 2*x[0] + x[1] - 1',
                                '-10*x[0]*x[1] + 4*x[0]*x[0] - 3*x[1]*x[1]/2.0 - 2*x[1] + x[0] + 5')
    verify_solpol(solpol, u, v)

