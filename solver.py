# encoding: utf8
from __future__ import division
import numpy
from dolfin import UnitSquareMesh, FunctionSpace, Function, Expression, TrialFunction, TestFunction
from dolfin import FacetNormal, Constant, dx, dS, ds, dot, jump, div
from dolfin import as_vector, interpolate, system, assemble, PETScKrylovSolver, PETScLUSolver
from solenoidal import SolenoidalLimiter
from orangutan import Log


def solve_transport_equation(N, P, dt, tmax, u_cpp, w_cpp, solver_method, logfile, limit):
    log = Log(logfile, console=True)
    
    ndim = 2 # 2D only
    
    # Mesh and function space
    mesh = UnitSquareMesh(N, N, 'right')
    V = FunctionSpace(mesh, 'DG', P)
    
    # The analytical expression for the convected velocity field
    u0_expr = Expression(u_cpp[0], degree=P, domain=mesh, t=0.0)
    u1_expr = Expression(u_cpp[0], degree=P, domain=mesh, t=0.0)
    ua = [u0_expr, u1_expr]
    
    # The analytical expression for the convecting velocity field
    w0_expr = Expression(w_cpp[0], degree=P, domain=mesh, t=0.0)
    w1_expr = Expression(w_cpp[1], degree=P, domain=mesh, t=0.0)
    w = as_vector([w0_expr, w1_expr])
    
    # Unknown functions, next time step
    u0 = Function(V)
    u1 = Function(V)
    u_new = [u0, u1]
    
    # Initial condition
    u0_expr.t = u1_expr.t = 0.0
    u0p = interpolate(u0_expr, V)
    u1p = interpolate(u1_expr, V)
    up = [u0p, u1p]
    
    # Previous time step
    u0_expr.t = u1_expr.t = -dt
    u0pp = interpolate(u0_expr, V)
    u1pp = interpolate(u1_expr, V)
    upp = [u0pp, u1pp]
    
    # Give functions names and run some sanity checks 
    for name, func in (('u0', u0), ('u1', u1),
                       ('u0p', u0p), ('u1p', u1p),
                       ('u0pp', u0pp), ('u1pp', u1pp)):
        func.rename(name, name)
        arr = func.vector().get_local()
        assert all(numpy.isfinite(arr)), 'Function %s is not finite!' % name

    # Create the slope limiter for the divergence free field
    if limit:
        limiter = SolenoidalLimiter(u_new)
    
    # Define transport equation
    n = FacetNormal(mesh)
    
    # Trial and test functions
    u = TrialFunction(V)
    v = TestFunction(V)
    
    # Time coefficients
    c1, c2, c3 = Constant([3/2, -2, 1/2])
    df_dt = Constant(dt)
    
    a, L = [], []
    for d in range(ndim):
        # Downstream normal velocities
        w_nD = (dot(w, n) - abs(dot(w, n)))/2
        
        # Discontinuous Galerkin implementation of the advection equation
        eq = (c1*u + c2*up[d] + c3*upp[d])/df_dt*v*dx
        
        # Convection integrated by parts two times to bring back the original
        # div form (this means we must subtract and add all fluxes)
        eq += div(u*w)*v*dx
        
        # Replace downwind flux with upwind flux on downwind internal facets
        eq -= jump(w_nD*v)*jump(u)*dS
        
        # Replace downwind flux with upwind BC flux on downwind external facets
        # Subtract the "normal" downwind flux
        eq -= w_nD*u*v*ds
        # Add the boundary value upwind flux
        eq += w_nD*ua[d]*v*ds
        
        ai, Li = system(eq)
        a.append(ai)
        L.append(Li)
    
    # Storage
    A = assemble(a[0])
    b = assemble(L[0])
    
    # Solver
    if solver_method == 'lu':
        solver = PETScLUSolver('petsc')
    else:
        solver = PETScKrylovSolver(solver_method, 'default')
        solver.parameters['nonzero_initial_guess'] = True
        solver.parameters['relative_tolerance'] = 1e-15
        solver.parameters['absolute_tolerance'] = 1e-15
    
    # Time loop
    to_update = [u0_expr, u1_expr, w0_expr, w1_expr]
    t = 0
    while t + dt/2 < tmax:
        t += dt
        for expr in to_update:
            expr.t = t
        
        # Solve transport equations
        for d in range(ndim):
            assemble(a[d], tensor=A)
            assemble(L[d], tensor=b)
            solver.solve(A, u_new[d].vector(), b)
            
        if limit:
            limiter.run()
        
        # Reporting
        u0a = u0.vector().get_local()
        u1a = u1.vector().get_local()
        
        log.report('t', t, '%7.3f')
        log.report('u0min', u0a.min())
        log.report('u0max', u0a.max())
        log.report('u1min', u1a.min())
        log.report('u1max', u1a.max())
        log.report_timestep()
        
        # Update for next time step
        for d in range(ndim):
            upp[d].assign(up[d])
            up[d].assign(u_new[d])
    
    return u_new, ua
