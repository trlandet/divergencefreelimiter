# encoding: utf8
from __future__ import division
import dolfin
dolfin.parameters['plotting_backend'] = 'matplotlib'
from matplotlib import pyplot
from solver import solve_transport_equation

# Input
N = 40
dt = 0.0025
tmax = 1.0
P = 2

u_cpp = ['1.0*(-pow(tanh(100.0*sqrt(pow(x[0] - 0.25*cos(2*pi*t) - 0.5, 2) + pow(x[1] - 0.25*sin(2*pi*t) - 0.5, 2)) - 12.0), 2) + 1)'
         '*(x[1] - 0.25*sin(2*pi*t) - 0.5)/sqrt(pow(x[0] - 0.25*cos(2*pi*t) - 0.5, 2) + pow(x[1] - 0.25*sin(2*pi*t) - 0.5, 2) + DOLFIN_EPS)'
         ,
         '-1.0*(-pow(tanh(100.0*sqrt(pow(x[0] - 0.25*cos(2*pi*t) - 0.5, 2) + pow(x[1] - 0.25*sin(2*pi*t) - 0.5, 2)) - 12.0), 2) + 1)'
         '*(x[0] - 0.25*cos(2*pi*t) - 0.5)/sqrt(pow(x[0] - 0.25*cos(2*pi*t) - 0.5, 2) + pow(x[1] - 0.25*sin(2*pi*t) - 0.5, 2) + DOLFIN_EPS)']
w_cpp = ('-2*pi*(x[1] - 0.5)',
         '2*pi*(x[0] - 0.5)')

uh, ua = solve_transport_equation(N, P, dt, tmax, u_cpp, w_cpp, 'lu', 'circle.log', limit=True)
V = uh[0].function_space()
mesh = V.mesh()

pyplot.figure()
c = dolfin.plot(uh[0], mesh=mesh, title='uh0')
pyplot.colorbar(c)

pyplot.figure()
c = dolfin.plot(ua[0], mesh=mesh, title='ua0')
pyplot.colorbar(c)
pyplot.show()