Divergence free limiter
=======================

.. warning:: Work in progress, unfinished, may never be finished

A test of an idea for a slope limiter for divergence free vector fields that
are advected by a known transporting velocity field in a linear transport 
equation.

    ∂(u)/∂t + w⋅∇(u) = 0

The limiter maintains ∇⋅u = 0 and does not change the cell mean "u" or the
facet mean "u⋅n" while at the same time attempting to remove wriggles caused
by Gibbs phenomenom.

Some functionality depends on optimization where either a Python implementation
using SciPy or a C++ implementation using a bundled port of a small part of the
SciPy optimization routines is used. This bundled code is licensed under the new
(3-clause) BSD license as specified in the header of the bundled C++ file
solenoidal/cpp/scipy_optimize.h

Copyright Tormod Landet, 2016-
Licensed under the Apache 2.0 license
