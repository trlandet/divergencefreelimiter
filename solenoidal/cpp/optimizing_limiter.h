#include <iostream>
#include <vector>
#include <tuple>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <dolfin/mesh/Mesh.h>
#include <dolfin/fem/DofMap.h>
#include <dolfin/function/Function.h>
#include <dolfin/la/GenericVector.h>
#include "scipy_optimize.h"


namespace dolfin {


typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> MatrixXdR;
typedef Eigen::Map<MatrixXdR> MatrixXdRMap;
typedef Eigen::Map<const MatrixXdR> MatrixXdRCMap;
typedef std::size_t id;


/*
 * Cost function base class
 */
class CostFunction : public scipy_optimize::CostFunction
{
public:
  virtual void new_time_step(const std::vector<std::shared_ptr<Function> >& vec_field,
                             const bool trust_lagrange_values,
                             const int* limit_cell) {};
  virtual void set_cell(const id cell_id, const MatrixXdRCMap *C,
                        const MatrixXdRCMap *Rinv, Eigen::VectorXd *rhs) {};
  virtual bool cell_needs_limiting() { return true; }
  virtual double operator()(const Eigen::VectorXd &x, Eigen::VectorXd &gradient,
                            bool need_gradient=true) = 0;
};


const int NPOLY = 9;
const int GDIM = 2;
const int FREEDOFS = 4;


/*
 * The cell limiter loop. Runs once per time step for each vector field
 */
void optimizing_limiter(CostFunction &cf,
                        const std::vector<std::shared_ptr<Function> > vec_field,
                        const bool trust_lagrange_values,
                        const int* limit_cell,
                        const int Npoly, const int Ncells, const int Npoints,
                        const double* C, const double* R, const double* Rinv,
                        double* coefficients, double* cell_cost)
{
  assert(Npoly == NPOLY);
  Eigen::VectorXd x(FREEDOFS);
  Eigen::VectorXd r(Npoly);

  // Recalculate any internal state that may be present in the cost function
  cf.new_time_step(vec_field, trust_lagrange_values, limit_cell);

  for (id cid = 0; cid < Ncells; cid++)
  {
    if (limit_cell[cid] == 0) continue;

    // The current DG solenoidal coefficients and evaluation matrices
    const MatrixXdRCMap Cmat(&C[cid*Npoints*GDIM*Npoly], Npoints*GDIM, Npoly);
    const MatrixXdRCMap Rmat(&R[cid*Npoly*Npoly], Npoly, Npoly);
    const MatrixXdRCMap Rinvmat(&Rinv[cid*Npoly*Npoly], Npoly, Npoly);
    MatrixXdRMap coeffs(&coefficients[cid*Npoly], Npoly, 1);

    // Right hand side of the constraints
    r.noalias() = Rmat * coeffs;

    // Send cell info to the cost function
    cf.set_cell(cid, &Cmat, &Rinvmat, &r);

    // Check if we need any minimisation at all
    if (!cf.cell_needs_limiting()) continue;

    // Initial guess
    for (int i = 0; i < FREEDOFS; i++) x[i] = r[NPOLY - FREEDOFS + i];

    // Minimise the cost function
    auto res = scipy_optimize::fmin_bfgs(cf, x);
    x = res.final_x;

    // Copy back results
    for (int i = 0; i < FREEDOFS; i++) r[NPOLY - FREEDOFS + i] = x[i];
    coeffs.noalias() = Rinvmat * r;

    // Store the final cost
    cell_cost[cid] = res.final_cost;
  }
}


/*
 * Cost function "LocalExtrema" implementation
 */
class CostFuncLocalExtrema : public CostFunction
{
public:
  CostFuncLocalExtrema(const Mesh &mesh,
                       const double out_of_bounds_penalty_fac,
                       const double out_of_bounds_penalty_const)
  {
    _mesh = &mesh;
    setup_connectivity();
    _out_of_bounds_penalty_fac = out_of_bounds_penalty_fac;
    _out_of_bounds_penalty_const = out_of_bounds_penalty_const;
  }

  void new_time_step(const std::vector<std::shared_ptr<Function> >& vec_field,
                     const bool trust_lagrange_values,
                     const int* limit_cell) final override
  {
    // Set the vector fields
    _vec_field.resize(0);
    for (auto func : vec_field)
    {
      _vec_field.push_back(func.get());
    }
    
    // Get vec_field values on this processor
    _vec_field[0]->vector()->get_local(_dof_values_0);
    _vec_field[1]->vector()->get_local(_dof_values_1);

    _trust_lagrange_values = trust_lagrange_values;
    calculate_averages();
    calculate_targets(limit_cell);
  }

  void set_cell(const id cell_id, const MatrixXdRCMap *C, const MatrixXdRCMap *Rinv,
      Eigen::VectorXd *rhs) final override
  {
    _current_cell = cell_id;
    _Rinv = Rinv;
    _C = C;
    _rhs = rhs;
  }

  bool cell_needs_limiting() final override
  {
    return _cell_needs_limiting[_current_cell] != 0;
  }

  double operator()(const Eigen::VectorXd &x, Eigen::VectorXd &gradient,
                    bool need_gradient=true) final override
  {
    num_calls++;
    const MatrixXdRCMap &C = *_C;
    const MatrixXdRCMap &Rinv = *_Rinv;
    Eigen::VectorXd &r = *_rhs;

    // The number of DG Lagrange coordinates
    std::size_t M = C.rows() / GDIM;

    // Update RHS with the new values and compute new coefficients
    for (int i = 0; i < FREEDOFS; i++) r[i + NPOLY - FREEDOFS] = x[i];
    Eigen::VectorXd new_coeffs(Rinv * r);

    // Compute values at Lagrange coordinate points
    Eigen::VectorXd vals(C * new_coeffs);

    // Get cell dofs
    auto dm = _vec_field[0]->function_space()->dofmap();
    auto cell_dofs = dm->cell_dofs(_current_cell);

    const double pfac = _out_of_bounds_penalty_fac;
    const double padd = _out_of_bounds_penalty_const;
    double minval, maxval, target;
    bool has_bounds;
    double cost = 0.0;
    for (int idof = 0; idof < M; idof++)
    {
      // Find cost for vector component 0
      auto dof = cell_dofs[idof];
      std::tie(minval, target, maxval, has_bounds) = _targets_0[dof];
      double val = vals[idof];
      double scale = has_bounds ? maxval - minval : _cell_averages_0[_current_cell];
      double scale2 = std::max(scale, 1e-8);
      double c = pow((val - target) / scale2, 2);
      if (has_bounds)
      {
        if (minval > val + 1e-4)
          c += pow((val - minval) / scale2, 2) * pfac + padd;
        else if (maxval < val - 1e-4)
          c += pow((val - maxval) / scale2, 2) * pfac + padd;
      }
      cost += c;

      // Find cost for vector component 1
      std::tie(minval, target, maxval, has_bounds) = _targets_1[dof];
      val = vals[idof + M];
      scale = has_bounds ? maxval - minval : _cell_averages_1[_current_cell];
      scale2 = std::max(scale, 1e-8);
      c = pow((val - target) / scale2, 2);
      if (has_bounds)
      {
        if (minval > val + 1e-4)
          c += pow((val - minval) / scale2, 2) * pfac + padd;
        else if (maxval < val - 1e-4)
          c += pow((val - maxval) / scale2, 2) * pfac + padd;
      }
      cost += c;
    }

    // Update the provided gradient vector
    if (need_gradient)
        scipy_optimize::compute_gradient(*this, x, gradient, cost);

    return cost;
  }
private:
  // Data from outside
  const Mesh *_mesh;
  std::vector<const Function*> _vec_field;
  bool _trust_lagrange_values;
  double _out_of_bounds_penalty_fac;
  double _out_of_bounds_penalty_const;

  id _current_cell;
  const MatrixXdRCMap *_C;
  const MatrixXdRCMap *_Rinv;
  Eigen::VectorXd *_rhs;

  // Data we create and own
  std::vector<std::vector<std::vector<id> > > _neighbours;
  std::vector<double> _cell_averages_0;
  std::vector<double> _cell_averages_1;
  std::vector<double> _dof_values_0;
  std::vector<double> _dof_values_1;
  std::vector<std::tuple<double, double, double, bool> > _targets_0;
  std::vector<std::tuple<double, double, double, bool> > _targets_1;
  std::vector<char> _cell_needs_limiting;

  /*
   * For each cell and for each of the 6 dofs in this cell find
   * the ids of all the connected neighbours (not including the
   * cell itself
   * */
  void setup_connectivity()
  {
    auto &mesh = *_mesh;
    auto connectivity_CV = mesh.topology()(2, 0);
    auto connectivity_VC = mesh.topology()(0, 2);
    auto connectivity_CF = mesh.topology()(2, 1);
    auto connectivity_FC = mesh.topology()(1, 2);

    for (CellIterator cell(mesh, "regular"); !cell.end(); ++cell)
    {
      const id cid = cell->index();
      _neighbours.emplace_back();
      std::vector<std::vector<id> > &cell_neighbours = _neighbours[cid];

      // Gather cells connected to each of the vertices
      const unsigned int* vertices = connectivity_CV(cid);
      int nvert_for_cell = connectivity_CV.size_global(cid);
      assert(nvert_for_cell == 3);
      for (int iv = 0; iv < nvert_for_cell; iv++)
      {
        const id vid = vertices[iv];
        cell_neighbours.emplace_back();
        std::vector<id> &vert_neighbours = cell_neighbours[iv];

        // Look at cells connected to this vertex
        const unsigned int* cells = connectivity_VC(vid);
        const int ncells_for_vert = connectivity_VC.size_global(vid);
        for (int ic = 0; ic < connectivity_VC.size_global(vid); ic++)
        {
          const id nbid = cells[ic];
          if (nbid != cid) vert_neighbours.push_back(nbid);
        }
      }

      // Gather cells connected to each of the facets
      const unsigned int* facets = connectivity_CF(cid);
      int nfacets_for_cell = connectivity_CF.size_global(cid);
      assert(nfacets_for_cell == 3);
      for (int ifa = 0; ifa < nfacets_for_cell; ifa++)
      {
        const id fid = facets[ifa];
        cell_neighbours.emplace_back();
        std::vector<id> &facet_neighbours = cell_neighbours[ifa + 3];

        // Look at cells connected to this facet
        const unsigned int* cells = connectivity_FC(fid);
        const int ncells_for_facet = connectivity_FC.size_global(fid);
        for (int ic = 0; ic < connectivity_FC.size_global(fid); ic++)
        {
          const id nbid = cells[ic];
          if (nbid != cid) facet_neighbours.push_back(nbid);
        }
      }
    }
    // Resize to total number of cells (include ghost cells)
    _cell_averages_0.resize(mesh.num_cells());
    _cell_averages_1.resize(mesh.num_cells());
    _cell_needs_limiting.resize(mesh.num_cells());
  }

  /*
   * Calculate cell average values for all cells, not only "regular" cells
   * */
  void calculate_averages()
  {
    auto dm = _vec_field[0]->function_space()->dofmap();
    std::size_t Ncells = _cell_averages_0.size();
    for (id cid = 0; cid < Ncells; cid++)
    {
      auto cell_dofs = dm->cell_dofs(cid);
      assert(cell_dofs.size() == 6);
      id d0 = cell_dofs[3];
      id d1 = cell_dofs[4];
      id d2 = cell_dofs[5];
      _cell_averages_0[cid] = (_dof_values_0[d0] + _dof_values_0[d1] + _dof_values_0[d2]) / 3.0;
      _cell_averages_1[cid] = (_dof_values_1[d0] + _dof_values_1[d1] + _dof_values_1[d2]) / 3.0;
    }
  }

  /*
   * Calculate dof min, max and target values
   * */
  void calculate_targets(const int* limit_cell)
  {
    auto dm = _vec_field[0]->function_space()->dofmap();

    // Make sure our storage is the correct size
    auto num_dofs_total = _dof_values_0.size();
    _targets_0.resize(num_dofs_total);
    _targets_1.resize(num_dofs_total);

    std::size_t Ncells = _cell_averages_0.size();
    for (id cid = 0; cid < Ncells; cid++)
    {
      _cell_needs_limiting[cid] = 0;
      if (limit_cell[cid] == 0) continue;

      auto& cell_neighbours = _neighbours[cid];
      auto cell_dofs = dm->cell_dofs(cid);
      auto num_cell_dofs = cell_dofs.size();
      for (std::size_t idof = 0; idof < num_cell_dofs; idof++)
      {
        auto dof = cell_dofs[idof];
        auto& nbs = cell_neighbours[idof];
        if (nbs.size() == 0)
        {
          _targets_0[dof] = std::make_tuple(0.0, _dof_values_0[dof], 0.0, false);
          _targets_1[dof] = std::make_tuple(0.0, _dof_values_1[dof], 0.0, false);
          continue;
        }

        // Find allowable high and low values
        double lim_low0 = _cell_averages_0[cid];
        double lim_low1 = _cell_averages_1[cid];
        double lim_high0 = lim_low0;
        double lim_high1 = lim_low1;
        for (auto nb : nbs)
        {
          lim_low0 = std::min(lim_low0, _cell_averages_0[nb]);
          lim_low1 = std::min(lim_low1, _cell_averages_1[nb]);
          lim_high0 = std::max(lim_high0, _cell_averages_0[nb]);
          lim_high1 = std::max(lim_high1, _cell_averages_1[nb]);
        }

        // If the DG Lagrange values have been prelimited we can trust them
        if (_trust_lagrange_values)
        {
          lim_low0 = std::min(lim_low0, _dof_values_0[dof]);
          lim_low1 = std::min(lim_low1, _dof_values_1[dof]);
          lim_high0 = std::max(lim_high0, _dof_values_0[dof]);
          lim_high1 = std::max(lim_high1, _dof_values_1[dof]);
          // We have no way to keep track of cells needing limiters
          _cell_needs_limiting[cid] = 1;
        }

        // Compute targets
        double target0 = std::min(lim_high0, std::max(lim_low0, _dof_values_0[dof]));
        double target1 = std::min(lim_high1, std::max(lim_low1, _dof_values_1[dof]));

        // Record bounds and targets
        _targets_0[dof] = std::make_tuple(lim_low0, target0, lim_high0, true);
        _targets_1[dof] = std::make_tuple(lim_low1, target1, lim_high1, true);

        if (target0 != _dof_values_0[dof] || target1 != _dof_values_1[dof])
          _cell_needs_limiting[cid] = 1;
      }
    }
  }
};

} // end namespace dolfin
