# encoding: utf8
from __future__ import division
import os
import numpy
import scipy.optimize
import dolfin
from .solenoidal_polynomials import SolenoidalPolynomials
from .quadrature import get_quadrature_rule
from .cost_functions import COST_FUNCTIONS, OUT_OF_BOUNDS_PENALTY_FAC, OUT_OF_BOUNDS_PENALTY_CONST


class SolenoidalLimiter(object):
    def __init__(self, V, cost_function='LocalExtrema', use_cpp=True, cf_options=None):
        """
        Slope limiter for solenoidal (divergence free) vector fields
        that maintain the solenoidal properties also after limiting
        
        The argument vec_field is an iterable of dolfin Functions that
        each represent one component of the vector field
        
        :param dolfin.FunctionSpace V: The function space of the vector field components
            (not the vector field itself)
        :param str cost_function: The name of the cost function to use
        :param bool use_cpp: Two implementations exist, a reference Python
            implementation and a faster C++ implementation. This flag
            switches between the two
        :param Callable[[], None] prelimiter: A function to call before
            limiting, but after the solenoidal invariants have been calculated.
            This will cause the cost functions to trust the prelimited values,
            and the max_cost parameter can kick in to use the prelimiter values
            instead of the solenoidal limiter values if 
        :param dict cf_options: Options for the cost function
        """
        self.solpol = SolenoidalPolynomials(V)
        self._preprocess(V)
        
        # Marker for which cells to limit
        self.limit_cell = numpy.ones(self.num_cells_owned, bool)
        
        # Per cell costs
        self.cell_cost = numpy.zeros(self.num_cells_owned, float)
        
        # Options for the cost function
        if cf_options is None:
            cf_options = {}
        
        if use_cpp:
            self.optimizer = CppOptimizer(self, cost_function, cf_options)
        else:
            raise NotImplementedError("ERROR use_cpp=False has not updated in a while, will not currently run at all!")
            self.optimizer = PythonOptimizer(self, vec_field, cost_function, self.limit_cell,
                                             self.prelimiter, self.trust_lagrange_values,
                                             self.cell_cost, self.max_cost, cf_options, vec_field2)
    
    def _preprocess(self, V):
        """
        Calculate quadrature rules to be used in the limiter
        """
        mesh = V.mesh()
        self.mesh = mesh
        connectivity_FV = mesh.topology()(1, 0)
        connectivity_CV = mesh.topology()(2, 0)
        coords = mesh.coordinates()
        
        self.degree = V.ufl_element().degree()
        ndim = mesh.topology().dim()
        assert self.degree == 2
        assert ndim == 2
        
        # Get facet and cell quadrature points and weights
        facet_points, facet_weights = get_quadrature_rule(1, self.degree)
        cell_points, cell_weights = get_quadrature_rule(2, self.degree)
        
        # Data to precalculate
        Ncells = self.num_cells_owned = self.solpol.num_cells_owned
        Npol = self.solpol.num_polynomials
        self.facet_quadrature = [None] * Ncells
        self.cell_quadrature = [None] * Ncells
        self.R = numpy.zeros((Ncells, Npol, Npol))
        self.Rinv = numpy.zeros((Ncells, Npol, Npol))
        
        # Loop over all cells and store precalculated quadrature schemes
        for cell in dolfin.cells(mesh, 'regular'):
            cid = cell.index()
            
            # Get the cells's vertex coordinates
            icv0, icv1, icv2 = connectivity_CV(cid)
            xcv0, ycv0 = coords[icv0]
            xcv1, ycv1 = coords[icv1]
            xcv2, ycv2 = coords[icv2]
            
            # Find cell center assuming linear geometry
            cx = (xcv0 + xcv1 + xcv2)/3
            cy = (ycv0 + ycv1 + ycv2)/3
            
            # Get cell quadrature rules for computing cell averages of each of the 
            # vector field components
            scale = cell.volume()
            cell_coefficients_x = numpy.zeros(Npol, float)
            cell_coefficients_y = numpy.zeros(Npol, float)
            tmp = numpy.zeros(Npol*2, float)
            for pt, wt in zip(cell_points, cell_weights):
                L0, L1 = pt
                L2 = 1 - L0 - L1
                
                # Convert from barycentric coordinates to physical x and y
                x = L0*xcv0 + L1*xcv1 + L2*xcv2
                y = L0*ycv0 + L1*ycv1 + L2*ycv2
                
                # Add the effect of this quadrature point to the total facet flux
                pos = (x - cx, y - cy)
                self.solpol.evaluate_basis_all(tmp, pos)
                cell_coefficients_x += tmp[:Npol] * wt * scale
                cell_coefficients_y += tmp[Npol:] * wt * scale
            self.cell_quadrature[cid] = cell_coefficients_x, cell_coefficients_y
            
            # Get quadrature rules for the facets in the cell to calculate the mean
            # flux of the vector field over each facets
            facet_quad_data = []
            for facet in dolfin.facets(cell):
                fid = facet.index()
                
                # Get the facet's vertex coordinates
                ifv0, ifv1 = connectivity_FV(fid)
                xfv0, yfv0 = coords[ifv0]
                xfv1, yfv1 = coords[ifv1]
                
                # Get the facet length and normal
                f0 = (xfv1 - xfv0)
                f1 = (yfv0 - yfv1)
                facet_length = (f0**2 + f1**2)**0.5
                n0 =  f1/facet_length
                n1 = -f0/facet_length
                
                coefficients = numpy.zeros(Npol, float)
                for pt, wt in zip(facet_points, facet_weights):
                    # Transform from reference quadrature rule coordinate pt ∈ [-1, 1]
                    # to physical x and y
                    fac = (1 + pt)/2
                    x = xfv0*fac + xfv1*(1-fac)
                    y = yfv0*fac + yfv1*(1-fac)
                    
                    # Scale the quadrature weight according to the facet and reference
                    # element lengths (Jacobian)
                    wt_scale = wt*facet_length/2
                    
                    # Add the effect of this quadrature point to the total facet flux
                    pos = (x - cx, y - cy)
                    self.solpol.evaluate_basis_all(tmp, pos)
                    coefficients += tmp[:Npol] * wt_scale * n0
                    coefficients += tmp[Npol:] * wt_scale * n1
                
                facet_quad_data.append(coefficients)
            self.facet_quadrature[cid] = facet_quad_data
            
            # Build the linear system of constraints
            R = numpy.eye(Npol, dtype=float)
            
            # 1) The mean value of the vector field components should not change
            R[0] = cell_coefficients_x
            R[1] = cell_coefficients_y
            
            # 2) The mean flux across facet "i" should not change
            for i, quad_coeffs in enumerate(facet_quad_data):
                R[i + 2] = quad_coeffs
            
            self.R[cid] = R
            self.Rinv[cid] = numpy.linalg.inv(R)
    
    def set_invariants(self, bdm_vec_field):
        """
        Convert from DG Lagrange to DG solenoidal polynomial basis
        and compute the invariants that must not be changed in the
        limiting process

        The given bdm_vec_field is assumed to be in a divergence
        conforming space with zero divergence, continuous fluxes
        and correct BCs. It should be stored in a normal nodal DG 
        Lagrange dolfin function.
        """
        u, v = bdm_vec_field
        self.solpol.set_from_lagrange(u, v)
    
    def run(self, vec_field, trust_lagrange_values, max_cost):
        """ 
        Run the optimizing solenoidal limiter
        """
        self.cell_cost[:] = 0
        u, v = vec_field
        self.optimizer.run([u, v], trust_lagrange_values, self.limit_cell, self.cell_cost)
        self.reuse(vec_field, max_cost)
    
    def reuse(self, vec_field, max_cost):
        """
        Reuse the optimized values to limit additional fields with
        different max_cost parameters only
        """
        skip_cells = None
        if max_cost is not None and max_cost < 1e100:
            skip_cells = self.cell_cost > max_cost
        
        # Convert back to DG
        u, v = vec_field
        self.solpol.get_lagrange(u, v, skip_cells)


class CppOptimizer(object):
    def __init__(self, solenoidal_limiter, cost_function_name, cf_options):
        """
        Optimizer for the slope limiter, C++ implementation
        """
        self.sollim = solenoidal_limiter
        self.solpol = solenoidal_limiter.solpol
        self.trust_lagrange_values = False
        
        self.cpp = get_cpp_module('optimizing_limiter')
        self.mesh = solenoidal_limiter.mesh
        assert self.mesh.geometry().dim() == 2
        
        cost_functions = {'LocalExtrema': self.cpp.CostFuncLocalExtrema}
        if not cost_function_name in cost_functions:
            err = 'C++ optimizer does not support cost function %s,' % cost_function_name
            err += ' only %r are supported' % sorted(cost_functions)
            raise ValueError(err)
        
        # Create the cost function
        CostFunc = cost_functions[cost_function_name]
        out_of_bounds_penalty_fac = cf_options.get('out_of_bounds_penalty_fac',
                                                   OUT_OF_BOUNDS_PENALTY_FAC)
        out_of_bounds_penalty_const = cf_options.get('out_of_bounds_penalty_const',
                                                     OUT_OF_BOUNDS_PENALTY_CONST) 
        self.cost_function = CostFunc(self.mesh,
                                      out_of_bounds_penalty_fac,
                                      out_of_bounds_penalty_const)
    
    def run(self, vec_field, trust_lagrange_values, limit_cell, cell_cost):
        """
        Convert to solenoidal basis, perform limiting for each cell and convert back
        C++ implementation using the bundled CppOptimizationLibrary
        """
        limit_cell = numpy.asarray(limit_cell, dtype=numpy.intc)
        Npoly = self.solpol.num_polynomials
        Ncell = self.solpol.num_cells_owned
        Npoints = self.solpol.C.shape[1] // self.mesh.geometry().dim()
        C = self.solpol.C
        R = self.sollim.R
        Rinv = self.sollim.Rinv
        coefficients = self.solpol.coefficients
        
        # Run the optimizing limiting in the solenoidal space
        self.cpp.optimizing_limiter(self.cost_function, vec_field, trust_lagrange_values, limit_cell,
                                    Npoly, Ncell, Npoints, C, R, Rinv, coefficients, cell_cost)


class PythonOptimizer(object):
    def __init__(self, solenoidal_limiter, vec_field, cost_function_name, limit_cell, prelimiter,
                 trust_lagrange_values, cell_cost, max_cost, cf_options, vec_field2):
        """
        Optimizer for the slope limiter, Python implementation
        """
        self.sollim = solenoidal_limiter
        self.solpol = solenoidal_limiter.solpol
        self.vec_field = vec_field
        self.vec_field2 = vec_field2
        self.limit_cell = limit_cell
        self.prelimiter = prelimiter
        self.cell_cost = cell_cost
        self.max_cost = max_cost
        
        # Get the cost function
        CostFunction = COST_FUNCTIONS[cost_function_name]
        self.cost_function = CostFunction(solenoidal_limiter, trust_lagrange_values, **cf_options)
    
    def run(self):
        """
        Convert to solenoidal basis, perform limiting for each cell and convert back
        Python implementation using scipy.optimize
        """
        # Convert from DG Lagrange to DG solenoidal polynomial basis 
        u, v = self.vec_field
        
        self.solpol.set_from_lagrange(u, v)
        self.prelimiter()
        
        self.cost_function.new_time_step(self.limit_cell)
        Npol = self.solpol.num_polynomials
        Ncells = self.solpol.num_cells_owned
        R = self.sollim.R
        Rinv = self.sollim.Rinv
        
        r = numpy.zeros(Npol, float)
        coefficients = self.solpol.coefficients
        for cid in xrange(Ncells):
            if not self.limit_cell[cid]:
                continue
            
            # The current DG solenoidal coefficients
            solpol_coeffs = coefficients[cid]
            
            # Right hand side of the constraints
            r[:5] = R[cid][:5].dot(solpol_coeffs)
            
            # Minimization
            minargs = {'args': (cid, r),  'full_output': True, 'disp': False}
            
            x0 = solpol_coeffs[5:]
            final_x, final_cost = scipy.optimize.fmin(self.cost_function, x0, **minargs)[:2]
            
            r[5:] = final_x
            new_coeffs = Rinv[cid].dot(r)
            coefficients[cid] = new_coeffs
            
            self.cell_cost[cid] = final_cost
        
        # Vec field 2 is always completely solenoidal, no max cost 
        if self.vec_field2 is not None:
            wu, wv = self.vec_field2
            self.solpol.get_lagrange(wu, wv)
            
        # If max_cost == 0 we have nothing more to do
        if self.max_cost == 0:
            return
        
        skip_cells = None
        if numpy.isfinite(self.max_cost):
            skip_cells = self.cell_cost > self.max_cost
        
        # Convert back to DG
        self.solpol.get_lagrange(u, v, skip_cells)
    
    def plot_cost_function(self, x0, xend, cid, r, plot_range=(-10, 10), resolution=21):
        """
        Visualise the cost function
        """
        if cid in (0,1):
            return True
        from matplotlib import pyplot, ticker
        
        N = len(x0)
        r0, r1 = plot_range
        ranges = [numpy.linspace(min(r0, min(x0[i], xend[i]) - 1),
                                 max(r1, max(x0[i], xend[i]) + 1),
                                 resolution) for i in range(N)]
        
        min_info, min_cost = None, 1e100
        for i0 in xrange(N):
            for i1 in xrange(i0 + 1, N):
                if True:
                    c0r = ranges[i0]
                    c1r = ranges[i1]
                else:
                    e0, e1 = xend[i0], xend[i1]
                    c0r = numpy.linspace(e0 - 1, e0 + 1, resolution)
                    c1r = numpy.linspace(e1 - 1, e1 + 1, resolution)
                X, Y = numpy.meshgrid(c0r, c1r)
                
                Z = numpy.zeros((resolution, resolution))
                x_tmp = numpy.array(xend)
                for j in xrange(resolution):
                    for k in xrange(resolution):
                        x_tmp[i0] = X[j,k]
                        x_tmp[i1] = Y[j,k]
                        f = self.cost_function(x_tmp, cid, r)
                        Z[j,k] = f
                        if f < min_cost:
                            min_cost = f
                            min_info = x_tmp.copy(), X, Y, Z, i0, i1 
        
        fend = self.cost_function(xend, cid, r)
        x, X, Y, Z, i0, i1 = min_info
        
        pyplot.figure()
        pyplot.title('CID %d Slice %d-%d of cost function.\nEnd cost %g, mincost %g' % (cid, i0, i1, fend, min_cost))
        CS = pyplot.contourf(X, Y, Z, resolution*2) #, locator=ticker.LogLocator())
        #pyplot.contour(CS, colors='k')
        #pyplot.contour(CS, colors='w', linestyles=':')
        pyplot.plot([x0[i0]], [x0[i1]], color='w', marker='o')
        pyplot.plot([xend[i0]], [xend[i1]], color='g', marker='x')
        pyplot.plot([x[i0]], [x[i1]], color='y', marker='+')
        cbar = pyplot.colorbar(CS)
        cbar.ax.set_ylabel('cost function')
        pyplot.xlabel('c[%s]' % i0)
        pyplot.ylabel('c[%s]' % i1)
        pyplot.show()
        
        # Save to pvd
        p1 = dolfin.Point(ranges[0][0],  ranges[1][0],  ranges[2][0]) 
        p2 = dolfin.Point(ranges[0][-1], ranges[1][-1], ranges[2][-1]) 
        mesh = dolfin.BoxMesh(p1, p2, resolution, resolution, resolution)
        V = dolfin.FunctionSpace(mesh, 'CG', 1) 
        outer = self       
        class CostExpression(dolfin.Expression):
            def eval(self, value, pos):
                x_tmp[:3] = pos
                x_tmp[3] = t
                value[0] = outer.cost_function(x_tmp, cid, r)
        expr = CostExpression()
        xdmf = dolfin.XDMFFile(dolfin.mpi_comm_world(), 'cost_function.xdmf')
        for t in ranges[3]:
            u = dolfin.interpolate(expr, V)
            u.rename('cost', 'cost')
            xdmf.write(u, t)


def get_cpp_module(name):
    """
    Use the dolfin machinery to compile, wrap with swig and load a c++ module
    """
    # How to find the C++ code
    my_dir = os.path.dirname(os.path.abspath(__file__)) 
    source_dir = os.path.join(my_dir, 'cpp')
    header_files = ['%s.h' % name]
    source_files = []
    
    # Read sources
    header_sources = []
    for hpp_filename in header_files:
        hpp_filename = os.path.join(source_dir, hpp_filename)
        
        with open(hpp_filename, 'rt') as f:
            hpp_code = f.read()
        header_sources.append(hpp_code)
    
    # Load the module
    try:
        module = dolfin.compile_extension_module(code='\n\n\n'.join(header_sources),
                                                 source_directory=source_dir, 
                                                 sources=source_files,
                                                 include_dirs=[source_dir])
    except RuntimeError as e:
        comperr = "In instant.recompile: The module did not compile with command"
        if e.message.startswith(comperr):
            filename = e.message.split("'")[3]
            print 'FILENAME', filename
            text = open(filename).read()
            print text
        raise
        
    return module
