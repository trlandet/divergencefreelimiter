# encoding: utf8
from __future__ import division
import numpy
import dolfin
from .quadrature import get_quadrature_rule


COST_FUNCTIONS = {}
def register_cost_function(name):
    def register(cost_function_class):
        COST_FUNCTIONS[name] = cost_function_class
        return cost_function_class
    return register


OUT_OF_BOUNDS_PENALTY_FAC = 1000.0
OUT_OF_BOUNDS_PENALTY_CONST = 0.1


@register_cost_function('ValueDiff')
class CostFuncValueDiff(object):
    def __init__(self, solenoidal_limiter, trust_lagrange_values):
        """
        The cost is the summed distance from the cell average values
        for both the velocity components at all DG Lagrange dof node
        locations. This should encourage relatively flat behaviour of
        the component functions. 
        """
        self.sollim = solenoidal_limiter
        self.solpol = solenoidal_limiter.solpol
        
    def new_time_step(self, *argv, **kwargs):
        pass
    
    def __call__(self, x, cid, r):
        r[5:] = x
        new_coeffs = self.sollim.Rinv[cid].dot(r)
        
        # Matrix of expansion coefficients at the DG Lagrange dof coordinates
        # and the number M of such coordinates 
        C = self.solpol.C[cid]
        M = C.shape[0]//2
        
        # Compute cost == summed distance from average values for both velocity
        # components at all DG Lagrange dof coordinates
        ux_avg, uy_avg = r[:2]
        cost = 0
        for i in range(M):
            ux = C[i + 0].dot(new_coeffs)
            uy = C[i + M].dot(new_coeffs)
            cost += abs(ux - ux_avg) + abs(uy - uy_avg)
        
        return cost


@register_cost_function('AbsGrad')
class CostFuncAbsGrad(object):
    def __init__(self, solenoidal_limiter, trust_lagrange_values):
        """
        The cost is absolute value of the gradient, ∫|∇u|dx 
        """
        self.sollim = solenoidal_limiter
        self.solpol = solenoidal_limiter.solpol
        self.quadrature = None
        self.quadrature_rule = get_quadrature_rule(2, self.sollim.degree)
        
    def new_time_step(self, *argv, **kwargs):
        pass
    
    def __call__(self, x, cid, r):
        r[5:] = x
        new_coeffs = self.sollim.Rinv[cid].dot(r)
        
        coords = self.sollim.mesh.coordinates()
        connectivity_CV = self.sollim.mesh.topology()(2, 0)
        Npol = self.solpol.num_polynomials
        
        # Get the cells's vertex coordinates
        icv0, icv1, icv2 = connectivity_CV(cid)
        xcv0, ycv0 = coords[icv0]
        xcv1, ycv1 = coords[icv1]
        xcv2, ycv2 = coords[icv2]
        
        # Find cell center assuming linear geometry
        cx = (xcv0 + xcv1 + xcv2)/3
        cy = (ycv0 + ycv1 + ycv2)/3
        
        # Perform cell quadrature of "cost = ∫|∇u|dx"
        area = 0.5*((xcv1 - xcv0)*(ycv2 - ycv0) + (xcv2 - xcv0)*(ycv1 - ycv0))
        scale = area
        cell_points, cell_weights = self.quadrature_rule
        tmp = numpy.zeros(Npol*4, float)
        cost = 0
        for pt, wt in zip(cell_points, cell_weights):
            L0, L1 = pt
            L2 = 1 - L0 - L1
            
            # Convert from barycentric coordinates to physical x and y
            x = L0*xcv0 + L1*xcv1 + L2*xcv2
            y = L0*ycv0 + L1*ycv1 + L2*ycv2
            
            # Perform quadrature
            pos = (x - cx, y - cy)
            self.solpol.evaluate_basis_derivatives_all(tmp, pos)
            dux_dx, dux_dy = tmp[Npol*0:Npol*1], tmp[Npol*1:Npol*2]
            duy_dx, duy_dy = tmp[Npol*2:Npol*3], tmp[Npol*3:Npol*4]

            # Perform quadrature
            cost += abs(dux_dx.dot(new_coeffs))**2 * wt * scale
            cost += abs(dux_dy.dot(new_coeffs))**2 * wt * scale
            cost += abs(duy_dx.dot(new_coeffs))**2 * wt * scale
            cost += abs(duy_dy.dot(new_coeffs))**2 * wt * scale
        
        return cost


@register_cost_function('LocalExtrema')
class CostFuncLocalExtrema(object):
    def __init__(self, solenoidal_limiter, trust_lagrange_values,
                 out_of_bounds_penalty_fac=OUT_OF_BOUNDS_PENALTY_FAC,
                 out_of_bounds_penalty_const=OUT_OF_BOUNDS_PENALTY_CONST):
        """
        The cost is absolute value of the distance from the target value
        which is the original value bounded to the allowable minimum and
        maximum values. If the value is out of bounds a heavy penalty is
        assigned.
        """
        self.sollim = solenoidal_limiter
        self.solpol = solenoidal_limiter.solpol
        self.mesh = solenoidal_limiter.mesh
        self.setup_connectivity()
        self.trust_lagrange_values = trust_lagrange_values
        self.out_of_bounds_penalty_fac = out_of_bounds_penalty_fac
        self.out_of_bounds_penalty_const = out_of_bounds_penalty_const
        
    def new_time_step(self, limit_cell):
        """
        Compute values that can be reused on all calls to the cost
        function on a given time step
        """
        self.calculate_averages()
        self.calculate_targets(limit_cell)
        
    def setup_connectivity(self):
        """
        Get neighbour cell ids for each dof location. For a vertex
        dof this can be arbitrarily many, for a facet dof it can be
        only one or zero cells. The cell to shich the dof belongs
        is not included in the neighbours
        """
        mesh = self.mesh
        connectivity_CV = self.sollim.mesh.topology()(2, 0)
        connectivity_VC = self.sollim.mesh.topology()(0, 2)
        connectivity_CF = self.sollim.mesh.topology()(2, 1)
        connectivity_FC = self.sollim.mesh.topology()(1, 2)
        
        self.neighbour_cells = []
        for cell in dolfin.cells(mesh, 'regular'):
            cid = cell.index()
            
            # Collect multiple neighbours for each of the three vertices
            # and one neighbour for each of the three facets 
            neighbours = []
            
            # Cells connected to vertices
            for vid in connectivity_CV(cid):
                neighbours.append([nbid for nbid in connectivity_VC(vid) if nbid != cid])
                
            # Cells connected to facets
            for fid in connectivity_CF(cid):
                neighbours.append([nbid for nbid in connectivity_FC(fid) if nbid != cid])
            
            assert cid == len(self.neighbour_cells)
            self.neighbour_cells.append(neighbours)
    
    def calculate_averages(self):
        """
        Compute cell averages of the vector field components for all
        cells in the mesh
        """
        vec_field = self.sollim.vec_field
        
        dm = vec_field[0].function_space().dofmap()
        values_0 = vec_field[0].vector().get_local()
        values_1 = vec_field[1].vector().get_local()
        
        Ncells = self.mesh.num_cells()
        self.cell_averages_0 = [None] * Ncells
        self.cell_averages_1 = [None] * Ncells
        
        for cid in xrange(Ncells):
            # Get dofs that contributes to the cell average
            # This only works for order 1 and 2!
            d0, d1, d2 = dm.cell_dofs(cid)[-3:]
            
            self.cell_averages_0[cid] = (values_0[d0] + values_0[d1] + values_0[d2]) / 3
            self.cell_averages_1[cid] = (values_1[d0] + values_1[d1] + values_1[d2]) / 3
    
    def calculate_targets(self, limit_cell):
        """
        Compute dof location value targets and the allowable dof
        value bounds
        """
        vec_field = self.sollim.vec_field
        
        dm = vec_field[0].function_space().dofmap()
        values_0 = vec_field[0].vector().get_local()
        values_1 = vec_field[1].vector().get_local()
        averages_0 = self.cell_averages_0
        averages_1 = self.cell_averages_1
        
        Ncells = self.mesh.num_cells()
        self.targets_0 = [None] * Ncells
        self.targets_1 = [None] * Ncells
        
        for cid in xrange(Ncells):
            if not limit_cell[cid]:
                continue
            
            dofs = dm.cell_dofs(cid)
            neighbour_cells = self.neighbour_cells[cid]
            cell_targets_0, cell_targets_1 = [], []
            
            # Get min, target and max values for each dof location in
            # this cell based on averages of this cell and neighbours
            for idof, dof in enumerate(dofs):
                nbs = neighbour_cells[idof]
                if len(nbs) == 0:
                    cell_targets_0.append((None, values_0[dof], None))
                    cell_targets_1.append((None, values_1[dof], None))
                    continue
                
                # Find the allowable high and low values
                lim_low0 = lim_high0 = averages_0[cid]
                lim_low1 = lim_high1 = averages_1[cid]
                for nb in nbs:
                    lim_low0 = min(lim_low0, averages_0[nb])
                    lim_low1 = min(lim_low1, averages_1[nb])
                    lim_high0 = max(lim_high0, averages_0[nb])
                    lim_high1 = max(lim_high1, averages_1[nb])
                    
                # If the DG Lagrange values have been prelimited we can trust them
                if self.trust_lagrange_values:
                    lim_low0 = min(lim_low0, values_0[dof])
                    lim_low1 = min(lim_low1, values_1[dof])
                    lim_high0 = max(lim_high0, values_0[dof])
                    lim_high1 = max(lim_high1, values_1[dof])
                    
                # Compute targets
                target0 = min(lim_high0, max(lim_low0, values_0[dof]))
                target1 = min(lim_high1, max(lim_low1, values_1[dof])) 
                
                # Record bounds and targets
                cell_targets_0.append((lim_low0, target0, lim_high0))
                cell_targets_1.append((lim_low1, target1, lim_high1))
            
            self.targets_0[cid] = cell_targets_0
            self.targets_1[cid] = cell_targets_1
    
    def __call__(self, x, cid, r):
        r[5:] = x
        new_coeffs = self.sollim.Rinv[cid].dot(r)
        
        targets_0 = self.targets_0[cid]
        targets_1 = self.targets_1[cid]
        
        # Matrix of expansion coefficients at the DG Lagrange dof coordinates
        # and the number M of such coordinates 
        C = self.solpol.C[cid]
        M = C.shape[0]//2
        assert M == len(targets_0)
        
        cfac = self.out_of_bounds_penalty_fac
        cadd = self.out_of_bounds_penalty_const
        EPS = 1e-8
        cost = 0
        vals = C.dot(new_coeffs)
        for i in range(M):
            # Find cost for vector component 0
            minval, target, maxval = targets_0[i]
            has_bounds = minval is not None
            val = vals[i]
            scale = maxval - minval if has_bounds else self.cell_averages_0[cid]
            scale2 = max(scale, 1e-8)
            c = ((val - target) / scale2)**2 / max(scale, 1e-8)
            if has_bounds:
                if minval - EPS*scale > val:
                    c += ((val - minval) / scale2)**2 * cfac + cadd
                elif maxval + EPS*scale < val:
                    c += ((val - maxval) / scale2)**2 * cfac + cadd
            cost += c
            
            # Find cost for vector component 1
            minval, target, maxval = targets_1[i]
            has_bounds = minval is not None
            val = vals[i + M]
            scale = maxval - minval if has_bounds else self.cell_averages_1[cid]
            scale2 = max(scale, 1e-8)
            c = ((val - target) / scale2)**2 / max(scale, 1e-8)
            if has_bounds:
                if minval - EPS*scale > val:
                    c += ((val - minval) / scale2)**2 * cfac + cadd
                elif maxval + EPS*scale < val:
                    c += ((val - maxval) / scale2)**2 * cfac + cadd
            cost += c
        
        return cost
