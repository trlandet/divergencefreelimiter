# encoding: utf8
from __future__ import division
import contextlib
import numpy
import dolfin


class SolenoidalPolynomials(object):
    def __init__(self, V):
        """
        Represent 2D DG vector functions as cell wise solenoidal polynomial
        coefficients. The input parameter V is the DG function that is to
        be converted from/to.
        
        The polynomials are as follows where x and y are distances to the
        cell center in directions of the standard cartesian axes
        
         1) (1, 0)
         2) (0, 1)
         
         3) (y, 0)
         4) (0, x)
         5) (x, -y)
         
         6) (y², 0)
         7) (0, x²)
         8) (x², -2xy)
         9) (-2xy, y²)
        
        As can be seen there are two zero order vector polynomials, three
        first order and four second order vector polynomials.
        """
        self.function_space = V
        self.degree = V.ufl_element().degree()
        self.mesh = V.mesh()
        
        assert self.degree == 2
        self._preprocess()
    
    def _preprocess(self):
        """
        Most work is performed offline as a preprocessor here. We assemble
        local matrices to convert to and from the solenoidal polynomials 
        """
        tdim = self.mesh.topology().dim()
        gdim = self.mesh.geometry().dim()
        assert gdim == tdim == 2
        self.num_cells_owned = self.mesh.topology().ghost_offset(tdim)

        # DG Lagrange velocity component function space
        V = self.function_space
        dm = V.dofmap()
        dofs_x = V.tabulate_dof_coordinates().reshape((-1, gdim))
        
        # Size and initialize the cell by cell matrices where we will store 
        # the results from this preprocessor
        Nc = self.num_cells_owned
        NVu = 6 # Number of dofs per cell in V
        Nso = 9 # Number of dofs per cell in solenoidal polynomial basis
        basis = numpy.zeros(gdim*Nso, float)
        pos = numpy.zeros(gdim, float)
        self.centers = numpy.zeros((Nc, 2), float)
        self.dof_coordinates = numpy.zeros((Nc, NVu, gdim), float)
        self.dofs = numpy.zeros((Nc, NVu), numpy.intc)
        
        # C is the solinoidal basis evaluated at the Lagrangian dof locations
        self.C = numpy.zeros((Nc, NVu*gdim, Nso), float)
        # Cplus is the pseudoinverse of C (least squares)
        self.Cplus = numpy.zeros((Nc, Nso, NVu*gdim), float)
        
        # Loop through cells and assemble matrices that are used to convert
        # between the DG Lagrange function spaces for the velocity components
        # and the DG solinoidal polynomial function space for the velocity.
        # The conversion can later be done efficiently cell by cell.
        for cell in dolfin.cells(self.mesh, 'regular'):
            cid = cell.index()
            
            # Find cell center assuming linear geometry
            coordinate_dofs = cell.get_coordinate_dofs()
            self.centers[cid,0] = cx = (coordinate_dofs[0] + coordinate_dofs[2] + coordinate_dofs[4])/3
            self.centers[cid,1] = cy = (coordinate_dofs[1] + coordinate_dofs[3] + coordinate_dofs[5])/3

            # Find dof coordinates
            dofs = dm.cell_dofs(cid)
            self.dofs[cid] = dofs
            self.dof_coordinates[cid] = dofs_x[dofs]
            
            # Get coeffients to evaluate the velocity from the stream function
            # at the vertices and the same for the velocity from the solinoidal
            # basis at the same points
            for idof in range(NVu):
                pos[0] = self.dof_coordinates[cid,idof,0]
                pos[1] = self.dof_coordinates[cid,idof,1]
                self.evaluate_basis_all(basis, (pos[0] - cx, pos[1] - cy))
                self.C[cid, idof + 0*NVu] = basis[:Nso]
                self.C[cid, idof + 1*NVu] = basis[Nso:]
            self.Cplus[cid] = numpy.linalg.pinv(self.C[cid])
        
        self.coefficients = numpy.zeros((Nc, Nso), float)
        self.num_polynomials = Nso
    
    def set_from_lagrange(self, u, v):
        """
        Find the solinoidal basis expansion coefficients from the given DG
        component functions
        """
        ua = u.vector().get_local()
        va = v.vector().get_local()
        N = self.dofs.shape[1]
        w = numpy.zeros(2*N, float)
    
        for cid in xrange(self.num_cells_owned):
            # Directly evaluate the velocity components at dof locations
            dofs = self.dofs[cid]
            w[:N] = ua[dofs]
            w[N:] = va[dofs]
            
            # Compute expansion coefficients
            self.coefficients[cid] = self.Cplus[cid].dot(w)
    
    def get_lagrange(self, u, v, skip_cells=None):
        """
        Update the given DG Lagrangian polynomial velocity components
        with the values from the solinoidal polynomials
        """
        ua = u.vector().get_local()
        va = v.vector().get_local()
        N = self.dofs.shape[1]
        for cid in xrange(self.num_cells_owned):
            if skip_cells is not None and skip_cells[cid]:
                continue
            dofs = self.dofs[cid]
            w = self.C[cid].dot(self.coefficients[cid])
            ua[dofs] = w[:N]
            va[dofs] = w[N:]
        u.vector().set_local(ua)
        v.vector().set_local(va)
        u.vector().apply('insert')
        v.vector().apply('insert')
    
    def evaluate_basis_all(self, results, x):
        """
        Evaluate the basis functions for both directions and store results
        in the results array. Evaluated at position x which is assumed to
        be given relative to the cell center
        """
        # X-direction
        results[0] = 1
        results[1] = 0
        results[2] = x[1]
        results[3] = 0
        results[4] = x[0]
        results[5] = x[1]**2
        results[6] = 0
        results[7] = x[0]**2
        results[8] = -2*x[0]*x[1]
        
        # Y-direction
        results[9] = 0
        results[10] = 1
        results[11] = 0
        results[12] = x[0]
        results[13] = -x[1]
        results[14] = 0
        results[15] = x[0]**2
        results[16] = -2*x[0]*x[1]
        results[17] = x[1]**2

    def evaluate_basis_derivatives_all(self, results, x):
        """
        Evaluate the basis functions for both directions and store results
        in the results array. Evaluated at position x which is assumed to
        be given relative to the cell center
        """
        # X-direction, X derivative
        results[0] = 0
        results[1] = 0
        results[2] = 0
        results[3] = 0
        results[4] = 1
        results[5] = 0
        results[6] = 0
        results[7] = 2*x[0]
        results[8] = -2*x[1]
        
        # X-direction, Y derivative
        results[9]  = 0
        results[10] = 0
        results[11] = 1
        results[12] = 0
        results[13] = 0
        results[14] = 2*x[1]
        results[15] = 0
        results[16] = 0
        results[17] = -2*x[0]
        
        # Y-direction, X derivative
        results[18] = 0
        results[19] = 0
        results[20] = 0
        results[21] = 1
        results[22] = 0
        results[23] = 0
        results[24] = 2*x[0]
        results[25] = -2*x[1]
        results[26] = 0
        
        # Y-direction, Y derivative
        results[27] = 0
        results[28] = 0
        results[29] = 0
        results[30] = 0
        results[31] = -1
        results[32] = 0
        results[33] = 0
        results[34] = -2*x[0]
        results[35] = 2*x[1]


@contextlib.contextmanager
def parameter_setter(params, vals):
    orig_vals = {}
    for key in vals:
        orig_vals[key] = params[key]
        params[key] = vals[key]
    yield
    for key in vals:
        params[key] = orig_vals[key]

